package ru.forwolk.home.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.forwolk.home.domain.Button;
import ru.forwolk.home.domain.Script;
import ru.forwolk.home.repository.ButtonRepository;

import java.util.Optional;

@Service
public class ButtonServiceImpl implements ButtonService {

    @Setter(onMethod_= @Autowired)
    private ScriptExecutorService scriptExecutorService;
    @Setter(onMethod_= @Autowired)
    private ButtonRepository buttonRepository;

    @Override
    public void onClick(int port, int mode, int count, int click) {
        Optional<Button> buttonOptional = buttonRepository.findButtonByPort(port);
        buttonOptional.ifPresent(b -> {
            Script script = b.getScript();
            script.getParams().put("clickContext", new ButtonClickContext(port, mode, count, click));
            scriptExecutorService.execute(script.getId());
        });
    }

    @Getter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    private static class ButtonClickContext {
        private int port;
        private int mode;
        private int count;
        private int click;
    }
}
