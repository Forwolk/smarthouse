package ru.forwolk.home.service.crud;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.forwolk.home.domain.Relay;
import ru.forwolk.home.mapper.RelayMapper;
import ru.forwolk.home.repository.RelayRepository;
import ru.forwolk.home.service.AbstractCrudService;
import ru.forwolk.home.web.dto.RelayDto;

@Service
public class RelayCrudService extends AbstractCrudService<RelayDto, Relay> {
    @Setter(onMethod_= @Autowired)
    @Getter
    private RelayRepository repository;
    @Setter(onMethod_= @Autowired)
    @Getter
    private RelayMapper mapper;
}
