package ru.forwolk.home.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.forwolk.home.domain.AuditMessage;

public interface AuditMessageRepository extends JpaRepository <AuditMessage, Integer> {
}
