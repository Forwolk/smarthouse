package ru.forwolk.home.domain;

public interface Identifiable {
    Integer getId();
}
