import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Modal from 'react-modal';
import Button from '@mui/material/Button';
import { Delete, Edit, Visibility } from '@mui/icons-material';
import { Fab } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { atomDark as syntaxHighlighterStyle } from "react-syntax-highlighter/dist/cjs/styles/prism";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";

const API_URL = 'http://localhost:8080/';

const Buttons = () => {
    const [buttons, setButtons] = useState([]);
    const [selectedButton, setSelectedButton] = useState(null);
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [isViewModalOpen, setIsViewModalOpen] = useState(false);
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
    const [isCreationMode, setIsCreationMode] = useState(false);
    const [scripts, setScripts] = useState([]);
    const [selectedScript, setSelectedScript] = useState(null);

    useEffect(() => {
        loadButtons();
        fetchScripts();
    }, []);

    // Функция для получения списка скриптов
    const fetchScripts = async () => {
        try {
            const response = await fetch(`${API_URL}/api/script`);
            const data = await response.json();
            if (data.response) {
                setScripts(data.response);
            }
        } catch (error) {
            console.error('Ошибка при получении списка скриптов:', error);
        }
    };

    const loadButtons = () => {
        axios.get(`${API_URL}api/button`)
            .then((response) => {
                setButtons(response.data.response);
            })
            .catch((error) => {
                console.error('Ошибка при загрузке списка кнопок:', error);
            });
    };

    const openEditModal = (button) => {
        setIsEditModalOpen(true);
        setIsCreationMode(false);
        setSelectedButton(button);
        setSelectedScript(scripts.find((script) => script.id === button.scriptId));
    }

    const openCreateModal = () => {
        setIsEditModalOpen(true);
        setIsCreationMode(true);
        setSelectedButton({});
    }

    const closeEditModal = () => {
        setIsEditModalOpen(false);
        setSelectedButton(null);
        setSelectedScript(null);
    }

    const openViewModal = (button) => {
        setIsViewModalOpen(true);
        setSelectedButton(button);
    }

    const closeViewModal = () => {
        setIsViewModalOpen(false);
        setSelectedButton(null);
    }

    const openDeleteModal = (button) => {
        setIsDeleteModalOpen(true);
        setSelectedButton(button);
    }

    const closeDeleteModal = () => {
        setIsDeleteModalOpen(false);
        setSelectedButton(null);
    }

    const handleCloseEditModalButton = () => {
        const confirmClose = window.confirm('Вы уверены, что хотите закрыть окно без сохранения изменений?');
        if (!confirmClose) return;
        closeEditModal();
    }

    const handleSaveEditModalButton = async () => {
        let method = isCreationMode ? 'POST' : 'PUT';

        try {
            const response = await fetch(`${API_URL}/api/button`, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify({
                    request: selectedButton,
                }),
            });

            const data = await response.json();
            if (data.status && data.status.code === 0) {
                setButtons((prevButtons) =>
                    prevButtons.filter((button) => button.id !== selectedButton.id)
                );
                setButtons((prevButtons) => [...prevButtons, data.response]);
            }

            closeEditModal();
        } catch (error) {
            console.error('Ошибка при сохранении кнопки:', error);
        }
    }

    const handleDeleteButton = async () => {
        try {
            const response = await fetch(`${API_URL}/api/button`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify({
                    request: selectedButton,
                }),
            });

            const data = await response.json();
            if (data.status && data.status.code === 0) {
                setButtons((prevButtons) =>
                    prevButtons.filter((button) => button.id !== selectedButton.id)
                );
            }

            closeDeleteModal();
        } catch (error) {
            console.error('Ошибка при удалении кнопки:', error);
        }
    }

    const handleButtonNameChange = (name) => {
        let button = selectedButton;
        setSelectedButton({
            "id": selectedButton.id,
            "name": name,
            "description": button.description,
            "port": button.port,
            "scriptId": button.scriptId
        });
    }

    const handleButtonDescriptionChange = (desc) => {
        let button = selectedButton;
        setSelectedButton({
            "id": selectedButton.id,
            "name": button.name,
            "description": desc,
            "port": button.port,
            "scriptId": button.scriptId
        });
    }

    const handleButtonPortChange = (port) => {
        let button = selectedButton;
        setSelectedButton({
            "id": selectedButton.id,
            "name": button.name,
            "description": button.description,
            "port": port,
            "scriptId": button.scriptId
        });
    }

    const handleButtonScriptChange = (scriptId) => {
        let button = selectedButton;
        setSelectedButton({
            "id": selectedButton.id,
            "name": button.name,
            "description": button.description,
            "port": button.port,
            "scriptId": scriptId
        });
        setSelectedScript(scripts.find((script) => script.id === scriptId));
    }

    return (
        <div>
            <h1>Раздел "Кнопки"</h1>
            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Порт</th>
                    <th>Описание</th>
                    <th>ID скрипта</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                {buttons.map((button) => (
                    <tr key={button.id}>
                        <td>{button.id}</td>
                        <td>{button.name}</td>
                        <td>{button.port}</td>
                        <td>{button.description}</td>
                        <td>{button.scriptId}</td>
                        <td>
                            <Button onClick={() => openViewModal(button)}>
                                <Visibility />
                            </Button>
                            <Button onClick={() => openEditModal(button)}>
                                <Edit />
                            </Button>
                            <Button onClick={() => openDeleteModal(button)}>
                                <Delete />
                            </Button>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>

            <Fab color="primary" aria-label="add" onClick={openCreateModal} style={{ position: 'fixed', bottom: '16px', right: '16px' }}>
                <AddIcon />
            </Fab>

            {/* Модальное окно просмотра деталей кнопки */}
            <Modal
                isOpen={isViewModalOpen}
                onRequestClose={closeViewModal}
                style={{
                    content: {
                        width: '60%',
                        top: '50%',
                        left: '55%',
                        transform: 'translate(-50%, -50%)',
                        minHeight: '300px',
                        height: '40%',
                        maxHeight: '80%',
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        padding: '20px',
                        borderRadius: '10px',
                        boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)',
                    },
                    overlay: {
                        backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        zIndex: '1000',
                    }
                }}
            >
                <div>
                    <h2>Детали кнопки</h2>
                    <p>ID: {selectedButton?.id}</p>
                    <p>Название: {selectedButton?.name}</p>
                    <p>Порт: {selectedButton?.port}</p>
                    <p>Описание: {selectedButton?.description}</p>
                    <p>ID скрипта: {selectedButton?.scriptId}</p>
                    <Button variant="contained" onClick={closeViewModal}>Закрыть</Button>{' '}
                    <Button variant="contained" color="primary">Переключить</Button>{' '}
                    <Button variant="contained" color="warning" onClick={() => {
                        let button = selectedButton;
                        closeViewModal();
                        openEditModal(button);
                    }}>Редактировать</Button>
                </div>
            </Modal>

            {/* Модальное окно редактирования/создания кнопки */}
            <Modal
                isOpen={isEditModalOpen}
                onRequestClose={handleCloseEditModalButton}
                style={{
                    content: {
                        width: '60%',
                        top: '50%',
                        left: '55%',
                        transform: 'translate(-50%, -50%)',
                        height: '80%',
                        maxHeight: '80%',
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        padding: '20px',
                        borderRadius: '10px',
                        boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)',
                    },
                    overlay: {
                        backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        zIndex: '1000',
                    }
                }}
            >
                {selectedButton && (
                    <div>
                        {isCreationMode ? (<h2>Создание кнопки</h2>) : (<h2>Редактирование кнопки</h2>)}
                        {!isCreationMode && (
                            <p>ID: {selectedButton?.id}</p>
                        )}
                        <input
                            type="text"
                            value={selectedButton.name}
                            onChange={(e) => handleButtonNameChange(e.target.value)}
                            placeholder="Введите название кнопки..."
                            style={{ width: '100%', padding: '8px', borderRadius: '5px', border: '1px solid #ccc', marginBottom: 10 }}
                        />
                        <input
                            type="number"
                            value={selectedButton.port}
                            onChange={(e) => handleButtonPortChange(e.target.value)}
                            placeholder="Введите порт кнопки..."
                            style={{ width: '100%', padding: '8px', borderRadius: '5px', border: '1px solid #ccc', marginBottom: 10 }}
                        />
                        <input
                            type="text"
                            value={selectedButton.description}
                            onChange={(e) => handleButtonDescriptionChange(e.target.value)}
                            placeholder="Введите описание кнопки..."
                            style={{ width: '100%', padding: '8px', borderRadius: '5px', border: '1px solid #ccc', marginBottom: 10 }}
                        />
                        <select
                            value={selectedButton.scriptId}
                            onChange={(e) => handleButtonScriptChange(parseInt(e.target.value))}
                            style={{ width: '100%', padding: '8px', borderRadius: '5px', border: '1px solid #ccc', marginBottom: 10 }}
                        >
                            <option value={0}>Выберите скрипт</option>
                            {scripts.map((script) => (
                                <option key={script.id} value={script.id}>{script.name}</option>
                            ))}
                        </select>
                        <Button variant="contained" color="primary" onClick={handleSaveEditModalButton}>Сохранить</Button>{' '}
                        <Button variant="contained" color="warning" onClick={handleCloseEditModalButton}>Закрыть</Button>{' '}

                        {/* Компонент SyntaxHighlighter для отображения кода скрипта */}
                        <SyntaxHighlighter
                            language="lua"
                            style={syntaxHighlighterStyle}
                            customStyle={{ width: '95%', height: '300px' }} // Задаем размеры textarea
                            showLineNumbers // Отображаем номера строк
                            wrapLines // Переносим строки, чтобы текст не выходил за пределы textarea
                            lineProps={{ style: { whiteSpace: 'pre-wrap' } }} // Разрешаем переносы строк внутри строки кода
                            codeTagProps={{ style: { fontFamily: 'inherit' } }} // Задаем наследование шрифта для кода
                        >
                            {selectedScript && selectedScript.source}
                        </SyntaxHighlighter>
                    </div>
                )}
            </Modal>

            {/* Модальное окно подтверждения удаления кнопки */}
            <Modal
                isOpen={isDeleteModalOpen}
                onRequestClose={closeDeleteModal}
                style={{
                    content: {
                        width: '40%',
                        height: 180,
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                    },
                    overlay: {
                        backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    }
                }}
            >
                {selectedButton && (
                    <div>
                        <h2>Удаление кнопки</h2>
                        <p>Вы уверены, что хотите удалить кнопку с ID {selectedButton.id}?</p>
                        <Button variant="contained" color="warning" onClick={handleDeleteButton}>Удалить</Button>{' '}
                        <Button variant="contained" color="primary" onClick={closeDeleteModal}>Отмена</Button>{' '}
                    </div>
                )}
            </Modal>
        </div>
    );
};

export default Buttons;
