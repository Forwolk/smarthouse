package ru.forwolk.home.event;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScriptExecutingEvent extends CancelableEvent {
    private String scriptName;
    private String script;

    public ScriptExecutingEvent(String scriptName, boolean audit) {
        super(audit);
        this.scriptName = scriptName;
    }
}
