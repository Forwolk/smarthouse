import React, {useEffect, useState} from 'react';
import {observer} from 'mobx-react-lite';
import axios from 'axios';
import Modal from 'react-modal';
import Button from '@mui/material/Button';
import {Delete, Edit, Visibility} from '@mui/icons-material';
import {Fab} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";

const API_URL = 'http://localhost:8080/';

const Relays = () => {
    const [relays, setRelays] = useState([]);
    const [selectedRelay, setSelectedRelay] = useState(null);
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [isViewModalOpen, setIsViewModalOpen] = useState(false);
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
    const [isCreationMode, setIsCreationMode] = useState(false);

    useEffect(() => {
        loadRelays();
    }, []);

    const loadRelays = () => {
        axios.get(`${API_URL}api/relay`)
            .then((response) => {
                setRelays(response.data.response);
            })
            .catch((error) => {
                console.error('Ошибка при загрузке списка реле:', error);
            });
    };

    const openEditModal = (relay) => {
        setIsEditModalOpen(true);
        setIsCreationMode(false);
        setSelectedRelay(relay);
    }

    const openCreateModal = () => {
        setIsEditModalOpen(true);
        setIsCreationMode(true);
        setSelectedRelay({});
    }

    const closeEditModal = () => {
        setIsEditModalOpen(false);
        setSelectedRelay(null);
    }

    const openViewModal = (relay) => {
        setIsViewModalOpen(true);
        setSelectedRelay(relay);
    }

    const closeViewModal = () => {
        setIsViewModalOpen(false);
        setSelectedRelay(null);
    }

    const openDeleteModal = (relay) => {
        setIsDeleteModalOpen(true);
        setSelectedRelay(relay);
    }

    const closeDeleteModal = () => {
        setIsDeleteModalOpen(false);
        setSelectedRelay(null);
    }

    const handleCloseEditModalButton = () => {
        const confirmClose = window.confirm('Вы уверены, что хотите закрыть окно без сохранения изменений?');
        if (!confirmClose) return;
        closeEditModal();
    }

    const handleSaveEditModalButton = async () => {
        let method = isCreationMode ? 'POST' : 'PUT';

        try {
            // Отправить запрос на создание скрипта на бэкенд
            const response = await fetch(`${API_URL}/api/relay`, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json', // Добавляем заголовок для указания принимаемого формата ответа
                },
                body: JSON.stringify({
                    request: selectedRelay,
                }),
            });

            const data = await response.json();
            if (data.status && data.status.code === 0) {
                setRelays((prevRelays) =>
                    prevRelays.filter((relay) => relay.id !== selectedRelay.id)
                );
                setRelays((prevRelays) => [...prevRelays, data.response]);
            }

            closeEditModal();
        } catch (error) {
            console.error('Error saving script:', error);
        }
    }

    const handleDeleteRelay = async () => {
        try {
            // Отправить запрос на создание скрипта на бэкенд
            const response = await fetch(`${API_URL}/api/relay`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json', // Добавляем заголовок для указания принимаемого формата ответа
                },
                body: JSON.stringify({
                    request: selectedRelay,
                }),
            });

            const data = await response.json();
            if (data.status && data.status.code === 0) {
                setRelays((prevRelays) =>
                    prevRelays.filter((relay) => relay.id !== selectedRelay.id)
                );
            }

            closeDeleteModal();
        } catch (error) {
            console.error('Error saving script:', error);
        }
    }

    const handleRelayNameChange = (name) => {
        let relay = selectedRelay;
        setSelectedRelay({
            "id": selectedRelay.id,
            "name": name,
            "description": relay.description,
            "port": relay.port
        });
    }

    const handleRelayDescriptionChange = (desc) => {
        let relay = selectedRelay;
        setSelectedRelay({
            "id": selectedRelay.id,
            "name": relay.name,
            "description": desc,
            "port": relay.port
        });
    }

    const handleRelayPortChange = (port) => {
        let relay = selectedRelay;
        setSelectedRelay({
            "id": selectedRelay.id,
            "name": relay.name,
            "description": relay.description,
            "port": port
        });
    }

    return (
        <div>
            <h1>Раздел "Реле"</h1>
            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Порт</th>
                    <th>Статус</th>
                    <th>Описание</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                {relays.map((relay) => (
                    <tr key={relay.id}>
                        <td>{relay.id}</td>
                        <td>{relay.name}</td>
                        <td>{relay.port}</td>
                        <td>{relay.active ? 'Вкл' : 'Выкл'}</td>
                        <td>{relay.description}</td>
                        <td>
                            <Button onClick={() => openViewModal(relay)}>
                                <Visibility/>
                            </Button>
                            <Button onClick={() => openEditModal(relay)}>
                                <Edit/>
                            </Button>
                            <Button onClick={() => openDeleteModal(relay)}>
                                <Delete/>
                            </Button>

                        </td>
                    </tr>
                ))}
                </tbody>
            </table>

            {/* Кнопка для создания реле */}
            <Fab color="primary" aria-label="add" onClick={openCreateModal}
                 style={{position: 'fixed', bottom: '16px', right: '16px'}}>
                <AddIcon/>
            </Fab>

            {/*Окно просмотра реле*/}
            <Modal
                isOpen={isViewModalOpen}
                onRequestClose={closeViewModal}
                style={{
                    content: {
                        width: '60%',
                        top: '50%',
                        left: '55%',
                        transform: 'translate(-50%, -50%)',
                        minHeight: '300px',
                        height: '40%',
                        maxHeight: '80%',
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        padding: '20px',
                        borderRadius: '10px',
                        boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)',
                    },
                    overlay: {
                        backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        zIndex: '1000',
                    }
                }}
            >
                <div>
                    <h2>Детали реле</h2>
                    <p>ID реле: {selectedRelay?.id}</p>
                    <p>Название: {selectedRelay?.name}</p>
                    <p>Порт: {selectedRelay?.port}</p>
                    <p>Статус: {selectedRelay?.active ? 'Вкл' : 'Выкл'}</p>
                    <p>Описание: {selectedRelay?.description}</p>
                    <Button variant="contained" onClick={closeViewModal}>Закрыть</Button>{' '}
                    <Button variant="contained" color="primary">Переключить</Button>{' '}
                    <Button variant="contained" color="warning" onClick={() => {
                        let relay = selectedRelay;
                        closeViewModal();
                        openEditModal(relay);
                    }}>Редактировать</Button>
                </div>
            </Modal>

            {/*Окно изменения реле*/}
            <Modal
                isOpen={isEditModalOpen}
                onRequestClose={handleCloseEditModalButton}
                style={{
                    content: {
                        width: '60%',
                        top: '50%',
                        left: '55%',
                        transform: 'translate(-50%, -50%)',
                        height: '80%',
                        maxHeight: '80%',
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        padding: '20px',
                        borderRadius: '10px',
                        boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)',
                    },
                    overlay: {
                        backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        zIndex: '1000',
                    }
                }}
            >
                {selectedRelay && (
                    <div>
                        {isCreationMode ? (<h2>Создание реле</h2>) : (<h2>Редактирование реле</h2>)}
                        {!isCreationMode && (
                            <p>ID реле: {selectedRelay?.id}</p>
                        )}
                        <input
                            type="text"
                            value={selectedRelay.name}
                            onChange={(e) => handleRelayNameChange(e.target.value)}
                            placeholder="Введите название реле..."
                            style={{width: '100%', padding: '8px', borderRadius: '5px', border: '1px solid #ccc', marginBottom: 10}}
                        />
                        <input
                            type="number"
                            value={selectedRelay.port}
                            onChange={(e) => handleRelayPortChange(e.target.value)}
                            placeholder="Введите порт реле..."
                            style={{width: '100%', padding: '8px', borderRadius: '5px', border: '1px solid #ccc', marginBottom: 10}}
                        />
                        <input
                            type="text"
                            value={selectedRelay.description}
                            onChange={(e) => handleRelayDescriptionChange(e.target.value)}
                            placeholder="Введите описание реле..."
                            style={{width: '100%', padding: '8px', borderRadius: '5px', border: '1px solid #ccc', marginBottom: 10}}
                        />
                        <Button variant="contained" color="primary" onClick={handleSaveEditModalButton}>Сохранить</Button>{' '}
                        <Button variant="contained" color="primary" onClick={() => {
                            let relay = selectedRelay;
                            closeViewModal();
                            openEditModal(relay);
                        }}>Редактировать</Button>{' '}
                        <Button variant="contained" color="warning" onClick={handleCloseEditModalButton}>Закрыть</Button>{' '}
                    </div>
                )}

            </Modal>

            {/* Модальное окно подтверждения удаления */}
            <Modal isOpen={isDeleteModalOpen} onRequestClose={closeDeleteModal}
                   style={{
                       content: {
                           width: '40%', // Устанавливаем 40% ширины окна
                           height: 180,
                           top: '50%', // Позиционируем окно по вертикали
                           left: '50%', // Позиционируем окно по горизонтали
                           transform: 'translate(-50%, -50%)', // Центрируем окно
                       },
                       overlay: {
                           backgroundColor: 'rgba(0, 0, 0, 0.5)', // Затеняем задний фон
                       }
                   }}
            >
                {selectedRelay && (
                    <div>
                        <h2>Удаление реле</h2>
                        <p>Вы уверены, что хотите удалить реле с ID {selectedRelay.id}?</p>
                        <Button variant="contained" color="warning" onClick={handleDeleteRelay}>Удалить</Button>{' '}
                        <Button variant="contained" color="primary" onClick={closeDeleteModal}>Отмена</Button>{' '}
                    </div>
                )}
            </Modal>
        </div>
    );
};

export default observer(Relays);
