package ru.forwolk.home.service.crud;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.forwolk.home.domain.Button;
import ru.forwolk.home.mapper.ButtonMapper;
import ru.forwolk.home.repository.ButtonRepository;
import ru.forwolk.home.service.AbstractCrudService;
import ru.forwolk.home.web.dto.ButtonDto;

@Service
public class ButtonCrudService extends AbstractCrudService<ButtonDto, Button> {
    @Setter(onMethod_= @Autowired)
    @Getter
    private ButtonRepository repository;
    @Setter(onMethod_= @Autowired)
    @Getter
    private ButtonMapper mapper;
}
