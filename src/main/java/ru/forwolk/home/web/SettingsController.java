package ru.forwolk.home.web;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.forwolk.home.config.ServiceConfiguration;
import ru.forwolk.home.domain.Property;
import ru.forwolk.home.exception.ApiException;
import ru.forwolk.home.web.dto.RequestDto;
import ru.forwolk.home.web.dto.ResponseDto;
import ru.forwolk.home.web.dto.StatusDto;

import java.util.List;

@Controller
@RequestMapping("admin/settings")
public class SettingsController extends AbstractController {

    @Setter(onMethod_= @Autowired)
    private ServiceConfiguration serviceConfiguration;

    @ResponseBody
    @PostMapping("property")
    public ResponseEntity<ResponseDto<String>> addSetting(@RequestBody RequestDto<Property> requestDto) {
        StatusDto statusDto;
        try {
            Property request = requestDto.getRequest();
            serviceConfiguration.setProperty(request.getKey(), request.getValue());
            statusDto = okStatus();
        } catch (ApiException apiException) {
            statusDto = createStatusFromException(apiException);
        }
        ResponseDto<String> responseDto = new ResponseDto<>();
        responseDto.setStatus(statusDto);
        return ResponseEntity.ok(responseDto);
    }


    @ResponseBody
    @GetMapping("property")
    public ResponseEntity<ResponseDto<List<Property>>> getSettings() {
        StatusDto statusDto;
        List<Property> properties = null;
        try {
            properties = serviceConfiguration.getAllProperties();
            statusDto = okStatus();
        } catch (ApiException apiException) {
            statusDto = createStatusFromException(apiException);
        }
        ResponseDto<List<Property>> responseDto = new ResponseDto<>();
        responseDto.setStatus(statusDto);
        responseDto.setResponse(properties);
        return ResponseEntity.ok(responseDto);
    }
}
