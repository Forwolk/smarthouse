package ru.forwolk.home.web;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.forwolk.home.service.ButtonService;

@Slf4j
@Controller
@RequestMapping("callback")
public class CallbackController {

    @Setter(onMethod_= @Autowired)
    private ButtonService buttonService;

    @GetMapping("/button")
    public ResponseEntity<String> buttonPress(
            @RequestParam(name = "pt") String port,
            @RequestParam(name = "m", required = false) String mode,
            @RequestParam(name = "cnt", required = false) String count,
            @RequestParam(name = "click", required = false) String click
    ) {
        log.info("REST request: on button press. port: {}, mode: {}, count: {}, click: {}", port, mode, count, click);
        buttonService.onClick(
                extract(port),
                extract(mode),
                extract(count),
                extract(click));
        return ResponseEntity.ok("OK");
    }

    private int extract(String s) {
        int result = -1;
        try {
            if (s != null) {
                result = Integer.parseInt(s);
            }
        } catch (NumberFormatException ignore) {

        }
        return result;
    }
}
