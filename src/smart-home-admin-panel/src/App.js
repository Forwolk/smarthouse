import React from 'react';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import Sidebar from './components/Sidebar';
import Home from './components/Home';
import Buttons from './components/Buttons';
import Scripts from './components/Scripts';
import Relays from './components/Relays';
import Settings from './components/Settings';
import Tasks from './components/Tasks';
import './App.css'
import ReactModal from 'react-modal';
import Test from "./components/Test";

ReactModal.setAppElement('#root');

const App = () => {
    return (
        <BrowserRouter>
            <div className="main-container">
                <div className="sidebar">
                    <Sidebar />
                </div>
                <div className="content">
                    <Routes>
                        {/* Маршрут для главной страницы */}
                        <Route path="/" element={<Home />} />
                        {/* Маршрут для страницы с кнопками */}
                        <Route path="/buttons" element={<Buttons />} />
                        {/* Маршрут для страницы со скриптами */}
                        <Route path="/scripts" element={<Scripts />} />
                        {/* Маршрут для страницы с реле */}
                        <Route path="/relays" element={<Relays />} />
                        {/* Маршрут для страницы с настройками */}
                        <Route path="/settings" element={<Settings />} />
                        {/* Маршрут для страницы с заданиями */}
                        <Route path="/tasks" element={<Tasks />} />
                        {/* Маршрут для страницы с заданиями */}
                        <Route path="/test" element={<Test />} />
                    </Routes>
                </div>
            </div>
        </BrowserRouter>
    );
};

export default App;