package ru.forwolk.home.config;

public final class SettingsKeys {
    private SettingsKeys() {
        throw new IllegalStateException("You are trying to create instance of Utility class");
    }

    public static final String VK_GROUP_ID = "smarthouse.vk.groupId";
    public static final String VK_ACCESS_TOKEN = "smarthouse.vk.accessToken";
    public static final String VK_USER_ID = "smarthouse.vk.userId";
}
