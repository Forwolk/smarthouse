package ru.forwolk.home.web.rest;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.forwolk.home.service.CrudService;
import ru.forwolk.home.web.dto.ButtonDto;
import ru.forwolk.home.web.dto.RequestDto;
import ru.forwolk.home.web.dto.ResponseDto;

@Slf4j
@Controller
@RequestMapping("/api/button")
@CrossOrigin(origins = "http://localhost:3000")
public class ButtonRestController extends AbstractRestController<ButtonDto> {
    @Setter(onMethod_= @Autowired)
    @Getter
    private CrudService<ButtonDto> service;

    @PostMapping
    @Override
    public ResponseEntity<ResponseDto<ButtonDto>> create(@RequestBody RequestDto<ButtonDto> buttonRequestDto) {
        return super.create(buttonRequestDto);
    }

    @PutMapping
    @Override
    public ResponseEntity<ResponseDto<ButtonDto>> update(@RequestBody RequestDto<ButtonDto> request) {
        return super.update(request);
    }

    @GetMapping
    @Override
    public ResponseEntity<ResponseDto<Iterable<ButtonDto>>> getAll() {
        return super.getAll();
    }

    @GetMapping(path = "/{id}")
    @Override
    public ResponseEntity<ResponseDto<ButtonDto>> get(@PathVariable("id") int id) {
        return super.get(id);
    }

    @DeleteMapping
    @Override
    public ResponseEntity<ResponseDto<Void>> delete(@RequestBody RequestDto<ButtonDto> request) {
        return super.delete(request);
    }
}
