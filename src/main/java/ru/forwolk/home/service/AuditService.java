package ru.forwolk.home.service;

import ru.forwolk.home.domain.AuditMessage;

import java.util.Map;

public interface AuditService {

    void audit(AuditMessage auditMessage);

    void audit(String eventType, String message);

    void audit(String eventType, String message, Map<String, String> parameters);
}
