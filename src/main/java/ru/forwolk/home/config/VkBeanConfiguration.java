package ru.forwolk.home.config;

import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VkBeanConfiguration {

    @Bean
    public VkApiClient vkApiClient() {
        TransportClient transportClient = HttpTransportClient.getInstance();
        return new VkApiClient(transportClient);
    }

    @Bean
    public GroupActor actor(
            @Value("${smarthouse.gateway.vk.groupId}") Integer groupId,
            @Value("${smarthouse.gateway.vk.accessToken}") String accessToken
    ) {
//        Integer groupId = serviceConfiguration.getInteger(SettingsKeys.VK_GROUP_ID);
//        String accessToken = serviceConfiguration.getString(SettingsKeys.VK_ACCESS_TOKEN);
        return new GroupActor(groupId, accessToken);
    }
}