package ru.forwolk.home.mapper;

import org.mapstruct.Mapper;
import ru.forwolk.home.domain.Script;
import ru.forwolk.home.web.dto.ScriptDto;

@Mapper(componentModel = "spring", uses = {})
public interface ScriptMapper extends EntityMapper<ScriptDto, Script> {

    default Script fromId(Integer id) {
        if (id == null) {
            return null;
        }
        Script script = new Script();
        script.setId(id);
        return script;
    }
}
