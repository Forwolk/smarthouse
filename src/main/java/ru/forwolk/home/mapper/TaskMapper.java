package ru.forwolk.home.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.forwolk.home.domain.Task;
import ru.forwolk.home.web.dto.TaskDto;

@Mapper(componentModel = "spring", uses = {ScriptMapper.class})
public interface TaskMapper extends EntityMapper<TaskDto, Task> {

    @Mapping(source = "script.id", target = "scriptId")
    @Mapping(source = "script.name", target = "scriptName")
    TaskDto toDto(Task task);

    @Mapping(source = "scriptId", target = "script")
    Task toEntity(TaskDto taskDTO);

    default Task fromId(Integer id) {
        if (id == null) {
            return null;
        }
        Task task = new Task();
        task.setId(id);
        return task;
    }
}
