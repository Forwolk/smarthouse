package ru.forwolk.home.web.rest;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.forwolk.home.service.CrudService;
import ru.forwolk.home.web.dto.TaskDto;
import ru.forwolk.home.web.dto.RequestDto;
import ru.forwolk.home.web.dto.ResponseDto;

import javax.websocket.server.PathParam;

@Slf4j
@Controller
@RequestMapping("/api/task")
@CrossOrigin(origins = "http://localhost:3000")
public class TaskRestController extends AbstractRestController<TaskDto> {
    @Setter(onMethod_= @Autowired)
    @Getter
    private CrudService<TaskDto> service;

    @PostMapping
    @Override
    public ResponseEntity<ResponseDto<TaskDto>> create(@RequestBody RequestDto<TaskDto> taskRequestDto) {
        return super.create(taskRequestDto);
    }

    @PutMapping
    @Override
    public ResponseEntity<ResponseDto<TaskDto>> update(@RequestBody RequestDto<TaskDto> request) {
        return super.update(request);
    }

    @GetMapping
    @Override
    public ResponseEntity<ResponseDto<Iterable<TaskDto>>> getAll() {
        return super.getAll();
    }

    @GetMapping(path = "/{id}")
    @Override
    public ResponseEntity<ResponseDto<TaskDto>> get(@PathParam("id") int id) {
        return super.get(id);
    }

    @DeleteMapping
    @Override
    public ResponseEntity<ResponseDto<Void>> delete(@RequestBody RequestDto<TaskDto> request) {
        return super.delete(request);
    }
}
