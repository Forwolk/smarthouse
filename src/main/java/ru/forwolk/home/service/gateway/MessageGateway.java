package ru.forwolk.home.service.gateway;

public interface MessageGateway {

    void sendMessage(String message);

    void sendMessage(int id, String message);
}
