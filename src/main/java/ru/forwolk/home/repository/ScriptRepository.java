package ru.forwolk.home.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.forwolk.home.domain.Script;

public interface ScriptRepository extends JpaRepository <Script, Integer> {
}
