import axios from 'axios';

const api = axios.create({
    baseURL: '/api', // Замените на URL вашего бэкенда, если он размещен на другом сервере
});

export default api;