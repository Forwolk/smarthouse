import React from 'react';
import { Link } from 'react-router-dom';
import { List, ListItem, ListItemIcon, ListItemText, Drawer } from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import PowerIcon from '@mui/icons-material/Power';
import BuildIcon from '@mui/icons-material/ToggleOn';
import ScriptIcon from '@mui/icons-material/Description';
import SettingsIcon from '@mui/icons-material/Settings';
import AssignmentIcon from '@mui/icons-material/Assignment';

const Sidebar = () => {
    return (
        <Drawer variant="permanent" anchor="left">
            <List>
                <ListItem button component={Link} to="/">
                    <ListItemIcon>
                        <HomeIcon />
                    </ListItemIcon>
                    <ListItemText primary="Главная" />
                </ListItem>
                <ListItem button component={Link} to="/buttons">
                    <ListItemIcon>
                        <BuildIcon />
                    </ListItemIcon>
                    <ListItemText primary="Кнопки" />
                </ListItem>
                <ListItem button component={Link} to="/relays">
                    <ListItemIcon>
                        <PowerIcon />
                    </ListItemIcon>
                    <ListItemText primary="Реле" />
                </ListItem>
                <ListItem button component={Link} to="/scripts">
                    <ListItemIcon>
                        <ScriptIcon />
                    </ListItemIcon>
                    <ListItemText primary="Скрипты" />
                </ListItem>
                <ListItem button component={Link} to="/tasks">
                    <ListItemIcon>
                        <AssignmentIcon />
                    </ListItemIcon>
                    <ListItemText primary="Задания" />
                </ListItem>
                <ListItem button component={Link} to="/settings">
                    <ListItemIcon>
                        <SettingsIcon />
                    </ListItemIcon>
                    <ListItemText primary="Настройки" />
                </ListItem>
            </List>
        </Drawer>
    );
};

export default Sidebar;
