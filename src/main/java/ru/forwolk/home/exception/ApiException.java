package ru.forwolk.home.exception;

import lombok.Getter;

@Getter
public class ApiException extends RuntimeException {
    private Integer code;
    private String message;
    private Exception cause;

    public ApiException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public ApiException(Integer code, String message, Exception cause) {
        this.code = code;
        this.message = message;
        this.cause = cause;
    }
}
