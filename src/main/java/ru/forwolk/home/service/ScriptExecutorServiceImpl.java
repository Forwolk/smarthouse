package ru.forwolk.home.service;

import com.google.common.eventbus.EventBus;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.forwolk.home.domain.Script;
import ru.forwolk.home.event.ScriptExecutingEvent;
import ru.forwolk.home.repository.ScriptRepository;
import ru.forwolk.home.service.gateway.MessageGateway;

import javax.annotation.PostConstruct;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Service
public class ScriptExecutorServiceImpl implements ScriptExecutorService {
    @Autowired
    private ScriptRepository scriptRepository;
    @Autowired
    private EventBus eventBus;
    @Autowired
    private GatewayService gatewayService;
    @Autowired
    private RelayService relayService;
    @Autowired
    private MessageGateway messageGateway;

    private final ExecutorService executorService = Executors.newFixedThreadPool(2);
    private ScriptEngineManager scriptEngineManager;

    @PostConstruct
    public void init() {
        scriptEngineManager = new ScriptEngineManager();
    }

    private ScriptEngine createScriptEngine() {
        ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("lua");
        scriptEngine.put("gateway", gatewayService);
        scriptEngine.put("messageGateway", messageGateway);
        scriptEngine.put("relayService", relayService);
        scriptEngine.put("log", new LoggerLuaProxy(log));
        return scriptEngine;
    }

    public void execute(int id) {
        Optional<Script> scriptOptional = scriptRepository.findById(id);
        if (scriptOptional.isPresent()) {
            Script script = scriptOptional.get();
            executorService.execute(() -> {
                try {
                    execute(script);
                } catch (Exception e) {
                    log.error("Error occurred while executing script '{}'", script.getName(), e);
                }
            });
        } else {
            log.warn("Script with id '{}' not found", id);
        }
    }

    public boolean executeSync(int id) throws Exception {
        Optional<Script> scriptOptional = scriptRepository.findById(id);
        if (scriptOptional.isPresent()) {
            Script script = scriptOptional.get();
            execute(script);
            return true;
        } else {
            return false;
        }
    }

    private void execute(Script script) throws Exception {
        log.info("Executing script '{}'", script.getName());
        ScriptExecutingEvent event = new ScriptExecutingEvent(script.getName(), false);
        eventBus.post(event);
        if (event.isCancelled()) {
            log.info("Executing script '{}' is cancelled", script.getName());
        } else {
            String scriptCode = script.getSource();
            ScriptEngine scriptEngine = createScriptEngine();
            if(!script.getParams().isEmpty()) {
                script.getParams().forEach(scriptEngine::put);
            }
            scriptEngine.eval(scriptCode);
        }
    }

    public void testScript(String scriptSource) throws Exception {
        Script script = new Script();
        script.setSource(scriptSource);
        execute(script);
    }

    private static class LoggerLuaProxy {
        private final Logger logger;

        private LoggerLuaProxy (Logger logger) {
            this.logger = logger;
        }

        public void info(String s) {
            logger.info(s);
        }

        public void warn(String s) {
            logger.warn(s);
        }

        public void error(String s) {
            logger.error(s);
        }
    }
}
