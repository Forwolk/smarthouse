package ru.forwolk.home.web.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RequestDto<T> implements Serializable {
    private T request;
}
