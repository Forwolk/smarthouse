package ru.forwolk.home.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import ru.forwolk.home.domain.Identifiable;
import ru.forwolk.home.exception.ApiException;
import ru.forwolk.home.service.CrudService;
import ru.forwolk.home.web.AbstractController;
import ru.forwolk.home.web.dto.RequestDto;
import ru.forwolk.home.web.dto.ResponseDto;

import java.util.Optional;

@Slf4j
public abstract class AbstractRestController<T extends Identifiable> extends AbstractController {

    public ResponseEntity<ResponseDto<T>> create(RequestDto<T> request) {
        T payload = request.getRequest();
        T t = getService().create(payload);
        return ResponseEntity.ok(createOkResponse(t));
    }

    public ResponseEntity<ResponseDto<T>> update(RequestDto<T> request) {
        T payload = request.getRequest();
        T t = getService().update(payload);
        return ResponseEntity.ok(createOkResponse(t));
    }

    public ResponseEntity<ResponseDto<T>> get(int id) {
        Optional<T> tOptional = getService().findById(id);
        if (tOptional.isPresent()) {
            return ResponseEntity.ok(createOkResponse(tOptional.get()));
        } else {
            throw new ApiException(404, "Entity with id " + id + " not found");
        }
    }

    public ResponseEntity<ResponseDto<Iterable<T>>> getAll() {
        Iterable<T> tIterable = getService().findAll();
        return ResponseEntity.ok(createOkResponse(tIterable));
    }

    public ResponseEntity<ResponseDto<Void>> delete(RequestDto<T> request) {
        getService().delete(request.getRequest());
        return ResponseEntity.ok(createOkResponse(null));
    }

    protected abstract CrudService<T> getService();
}
