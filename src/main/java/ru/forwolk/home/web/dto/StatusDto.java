package ru.forwolk.home.web.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class StatusDto implements Serializable {
    private Integer code;
    private String description;
    private Exception exception;
}
