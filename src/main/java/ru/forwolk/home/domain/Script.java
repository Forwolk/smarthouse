package ru.forwolk.home.domain;


import lombok.Data;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A Script.
 */
@Data
@Entity
@Table(name = "sh_script")
public class Script implements Serializable, Identifiable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Lob
    private String source;
    private transient final Map<String, Object> params = new HashMap<>();
}
