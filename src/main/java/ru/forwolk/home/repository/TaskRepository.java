package ru.forwolk.home.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.forwolk.home.domain.Relay;
import ru.forwolk.home.domain.Task;

public interface TaskRepository extends JpaRepository<Task, Integer> {
}
