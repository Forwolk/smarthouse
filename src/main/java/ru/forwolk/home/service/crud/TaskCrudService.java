package ru.forwolk.home.service.crud;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.forwolk.home.domain.Task;
import ru.forwolk.home.mapper.TaskMapper;
import ru.forwolk.home.repository.TaskRepository;
import ru.forwolk.home.service.AbstractCrudService;
import ru.forwolk.home.web.dto.TaskDto;

@Service
public class TaskCrudService extends AbstractCrudService<TaskDto, Task> {
    @Setter(onMethod_= @Autowired)
    @Getter
    private TaskRepository repository;
    @Setter(onMethod_= @Autowired)
    @Getter
    private TaskMapper mapper;
}
