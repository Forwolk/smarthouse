package ru.forwolk.home.event;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancelableEvent extends SmartHouseEvent {
    private boolean cancelled;

    protected CancelableEvent(boolean audit) {
        super(audit);
    }
}
