package ru.forwolk.home.web.dto;

import lombok.Data;
import ru.forwolk.home.domain.Identifiable;

@Data
public class ButtonDto implements Identifiable {
    private Integer id;
    private String name;
    private String description;
    private Integer port;
    private Integer scriptId;
    private String scriptName;
}
