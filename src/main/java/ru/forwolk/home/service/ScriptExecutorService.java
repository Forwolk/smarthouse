package ru.forwolk.home.service;

public interface ScriptExecutorService {
    void execute(int id);
    boolean executeSync(int id) throws Exception;
}
