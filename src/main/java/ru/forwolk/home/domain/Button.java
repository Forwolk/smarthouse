package ru.forwolk.home.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A Button.
 */
@Data
@Entity
@Table(name = "sh_button")
public class Button implements Serializable, Identifiable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    private Integer port;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Script script;

}
