package ru.forwolk.home.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.forwolk.home.exception.ApiException;
import ru.forwolk.home.web.dto.ResponseDto;
import ru.forwolk.home.web.dto.StatusDto;

@Slf4j
@ControllerAdvice
public class RestApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ApiException.class, RuntimeException.class})
    protected ResponseEntity<ResponseDto<Object>> handleApiException(RuntimeException ex, WebRequest request) {
        log.error("Error occurred while calling method '{}'", request.getContextPath(), ex);
        ResponseDto<Object> responseDto = new ResponseDto<>();

        if (ex instanceof ApiException) {
            responseDto.setStatus(createStatusFromException((ApiException) ex));
        } else {
            StatusDto statusDto = new StatusDto();
            statusDto.setCode(500);
            statusDto.setDescription("Internal server error");
            statusDto.setException(ex);
        }
        return ResponseEntity.status(responseDto.getStatus().getCode()).body(responseDto);
    }

    private StatusDto createStatusFromException(ApiException apiException) {
        StatusDto statusDto = new StatusDto();
        statusDto.setCode(apiException.getCode());
        statusDto.setDescription(apiException.getMessage());
        statusDto.setException(apiException.getCause());
        return statusDto;
    }
}
