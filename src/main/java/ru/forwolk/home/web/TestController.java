package ru.forwolk.home.web;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.forwolk.home.service.gateway.MessageGateway;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Controller
@RequestMapping("test")
public class TestController {
    @Setter(onMethod_= @Autowired)
    private MessageGateway messageGateway;

    @RequestMapping(path = "sendMessage", method = RequestMethod.POST)
    public ResponseEntity<String> sendMessage(@RequestBody String message) {
        messageGateway.sendMessage(message);
        return ResponseEntity.ok("Message send");
    }

    @RequestMapping(path = "ping", method = RequestMethod.GET)
    public ResponseEntity<String> sendMessage() {
        return ResponseEntity.ok("PONG");
    }

}
