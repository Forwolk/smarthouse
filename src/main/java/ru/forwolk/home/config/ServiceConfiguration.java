package ru.forwolk.home.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.forwolk.home.domain.Property;
import ru.forwolk.home.exception.PropertyNotFoundException;
import ru.forwolk.home.repository.PropertyRepository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class ServiceConfiguration {

    @Setter(onMethod_= @Autowired)
    private PropertyRepository propertyRepository;
    @Setter(onMethod_= @Autowired)
    private ObjectMapper objectMapper;

    @PostConstruct
    public void init() throws IOException {
        InputStream resourceAsStream = ServiceConfiguration.class.getResourceAsStream("/default-settings.json");
        String json = String.join("", IOUtils.readLines(resourceAsStream, StandardCharsets.UTF_8));
        List<Property> properties = objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, Property.class));

        log.info("Loading default properties");
        for (Property property : properties) {
            Optional<Property> propertyOptional = propertyRepository.findByKey(property.getKey());
            if (!propertyOptional.isPresent()) {
                propertyRepository.save(property);
            }
        }
    }

    public String getString(String key) {
        Optional<Property> byKey = propertyRepository.findByKey(key);

        return byKey
                .map(Property::getValue)
                .orElseThrow(() -> new PropertyNotFoundException("Property '" + key + "' is not found"));
    }

    public Integer getInteger(String key) {
        Optional<Property> byKey = propertyRepository.findByKey(key);

        return byKey
                .map(Property::getValue)
                .map(Integer::parseInt)
                .orElseThrow(() -> new PropertyNotFoundException("Property '" + key + "' is not found"));
    }

    public Boolean getBoolean(String key) {
        Optional<Property> byKey = propertyRepository.findByKey(key);

        return byKey
                .map(Property::getValue)
                .map(Boolean::parseBoolean)
                .orElseThrow(() -> new PropertyNotFoundException("Property '" + key + "' is not found"));
    }

    public void setProperty(String key, String value) {
        Property property = new Property();
        property.setKey(key);
        property.setValue(value);

        propertyRepository.save(property);
        log.info("Property [{}: '{}'] is saved", key, value);
    }

    public List<Property> getAllProperties() {
        return propertyRepository.findAll();
    }
}
