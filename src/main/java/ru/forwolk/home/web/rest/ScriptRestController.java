package ru.forwolk.home.web.rest;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.forwolk.home.service.CrudService;
import ru.forwolk.home.web.dto.ScriptDto;
import ru.forwolk.home.web.dto.RequestDto;
import ru.forwolk.home.web.dto.ResponseDto;

import javax.websocket.server.PathParam;

@Slf4j
@Controller
@RequestMapping("/api/script")
@CrossOrigin(origins = "http://localhost:3000")
public class ScriptRestController extends AbstractRestController<ScriptDto> {
    @Setter(onMethod_= @Autowired)
    @Getter
    private CrudService<ScriptDto> service;

    @PostMapping
    @Override
    public ResponseEntity<ResponseDto<ScriptDto>> create(@RequestBody RequestDto<ScriptDto> scriptRequestDto) {
        return super.create(scriptRequestDto);
    }

    @PutMapping
    @Override
    public ResponseEntity<ResponseDto<ScriptDto>> update(@RequestBody RequestDto<ScriptDto> request) {
        return super.update(request);
    }

    @GetMapping
    @Override
    public ResponseEntity<ResponseDto<Iterable<ScriptDto>>> getAll() {
        return super.getAll();
    }

    @GetMapping(path = "/{id}")
    @Override
    public ResponseEntity<ResponseDto<ScriptDto>> get(@PathParam("id") int id) {
        return super.get(id);
    }

    @DeleteMapping
    @Override
    public ResponseEntity<ResponseDto<Void>> delete(@RequestBody RequestDto<ScriptDto> request) {
        return super.delete(request);
    }
}
