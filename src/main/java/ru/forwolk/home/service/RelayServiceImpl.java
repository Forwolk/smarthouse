package ru.forwolk.home.service;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.forwolk.home.domain.Relay;
import ru.forwolk.home.repository.RelayRepository;

import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class RelayServiceImpl implements RelayService {

    @Setter(onMethod_= @Autowired)
    private GatewayService gatewayService;
    @Setter(onMethod_= @Autowired)
    private RelayRepository relayRepository;

    private final ScheduledExecutorService taskScheduler = Executors.newScheduledThreadPool(2);

    @Override
    public boolean isActive(int id) {
        Optional<Relay> relayOptional = relayRepository.findById(id);

        if (relayOptional.isPresent()) {
            return relayOptional.get().isActive();
        } else {
            return false;
        }
    }

    @Override
    public void activate(int id) {
        if (!isActive(id)) {
            forceActivate(id);
        }
    }

    @Override
    public void deactivate(int id) {
        if (isActive(id)) {
            forceDeactivate(id);
        }
    }

    private void forceActivate(int id) {
        Optional<Relay> relayOptional = relayRepository.findById(id);
        relayOptional.ifPresent(r -> {
            gatewayService.activate(r.getPort());
            r.setActive(true);
            relayRepository.save(r);
        });
    }

    private void forceDeactivate(int id) {
        Optional<Relay> relayOptional = relayRepository.findById(id);
        relayOptional.ifPresent(r -> {
            gatewayService.deactivate(r.getPort());
            r.setActive(false);
            relayRepository.save(r);
        });
    }



    @Override
    public void trigger(int id) {
        if (isActive(id)) {
            deactivate(id);
        } else {
            activate(id);
        }
    }

    @Override
    public void disableAll() {
        relayRepository.findAll().forEach(r -> {
            try {
                deactivate(r.getId().intValue());
                Thread.sleep(50);
            } catch (Exception e) {
                log.warn("Error occurred while deactivating relay", e);
            }
        });
    }

    @Override
    public void schedule(int id, int sec) {
        taskScheduler.schedule(() -> trigger(id), sec, TimeUnit.SECONDS);
    }
}
