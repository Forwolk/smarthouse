package ru.forwolk.home.service;

import ru.forwolk.home.domain.Identifiable;

import java.util.Optional;

public interface CrudService<T extends Identifiable> {
    T create(T t);
    T update (T t);
    Iterable<T> findAll();
    Optional<T> findById(Integer id);
    void delete(T t);
}
