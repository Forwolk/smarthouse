package ru.forwolk.home.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A Task.
 */
@Data
@Entity
@Table(name = "sh_task")
public class Task implements Serializable, Identifiable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    private String cron;
    @ManyToOne(optional = false)
    @JsonIgnoreProperties("")
    private Script script;
}
