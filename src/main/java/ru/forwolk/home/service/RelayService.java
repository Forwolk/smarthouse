package ru.forwolk.home.service;

public interface RelayService {
    boolean isActive(int id);

    void activate(int id);

    void deactivate(int id);

    void trigger(int id);

    void disableAll();

    void schedule(int id, int sec);
}
