package ru.forwolk.home.web.rest;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.forwolk.home.service.CrudService;
import ru.forwolk.home.web.dto.RelayDto;
import ru.forwolk.home.web.dto.RequestDto;
import ru.forwolk.home.web.dto.ResponseDto;

import javax.websocket.server.PathParam;

@Slf4j
@Controller
@RequestMapping("/api/relay")
@CrossOrigin(origins = "http://localhost:3000")
public class RelayRestController extends AbstractRestController<RelayDto> {
    @Setter(onMethod_= @Autowired)
    @Getter
    private CrudService<RelayDto> service;

    @PostMapping
    @Override
    public ResponseEntity<ResponseDto<RelayDto>> create(@RequestBody RequestDto<RelayDto> relayRequestDto) {
        return super.create(relayRequestDto);
    }

    @PutMapping
    @Override
    public ResponseEntity<ResponseDto<RelayDto>> update(@RequestBody RequestDto<RelayDto> request) {
        return super.update(request);
    }

    @GetMapping
    @Override
    public ResponseEntity<ResponseDto<Iterable<RelayDto>>> getAll() {
        return super.getAll();
    }

    @GetMapping(path = "/{id}")
    @Override
    public ResponseEntity<ResponseDto<RelayDto>> get(@PathParam("id") int id) {
        return super.get(id);
    }

    @DeleteMapping
    @Override
    public ResponseEntity<ResponseDto<Void>> delete(@RequestBody RequestDto<RelayDto> request) {
        return super.delete(request);
    }
}
