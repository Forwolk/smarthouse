package ru.forwolk.home.web;

import ru.forwolk.home.exception.ApiException;
import ru.forwolk.home.web.dto.ResponseDto;
import ru.forwolk.home.web.dto.StatusDto;

public abstract class AbstractController {

    protected StatusDto createStatusFromException(ApiException apiException) {
        StatusDto statusDto = new StatusDto();
        statusDto.setCode(apiException.getCode());
        statusDto.setDescription(apiException.getMessage());
        statusDto.setException(apiException.getCause());
        return statusDto;
    }

    protected StatusDto okStatus() {
        StatusDto statusDto = new StatusDto();
        statusDto.setCode(0);
        statusDto.setDescription("OK");
        return statusDto;
    }

    protected <T> ResponseDto<T> createOkResponse(T t) {
        ResponseDto<T> responseDto = new ResponseDto<>();
        responseDto.setResponse(t);
        responseDto.setStatus(okStatus());
        return responseDto;
    }
}
