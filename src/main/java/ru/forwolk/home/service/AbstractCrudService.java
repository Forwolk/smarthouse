package ru.forwolk.home.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.forwolk.home.domain.Identifiable;
import ru.forwolk.home.exception.ApiException;
import ru.forwolk.home.mapper.EntityMapper;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
public abstract class AbstractCrudService<D extends Identifiable, E> implements CrudService<D> {

    @Override
    public D create(D dto) {
        if (Objects.nonNull(dto.getId())) {
            throw new ApiException(400, "Object contains field 'id' on method create");
        }
        log.debug("Creating object '{}' of class '{}'", dto, dto.getClass());
        E entity = getMapper().toEntity(dto);
        entity = getRepository().save(entity);
        return getMapper().toDto(entity);
    }

    @Override
    public D update(D dto) {
        if (Objects.isNull(dto.getId())) {
            throw new ApiException(400, "Object does not contain field 'id' on method update");
        }
        log.debug("Updating object '{}' of class '{}'", dto, dto.getClass());
        E entity = getMapper().toEntity(dto);
        entity = getRepository().save(entity);
        return getMapper().toDto(entity);
    }

    @Override
    public List<D> findAll() {
        List<E> entities = getRepository().findAll();
        return getMapper().toDto(entities);
    }

    @Override
    public Optional<D> findById(Integer id) {
        Optional<E> entityOptional = getRepository().findById(id);
        if (entityOptional.isPresent()) {
            D dto = getMapper().toDto(entityOptional.get());
            return Optional.of(dto);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void delete(D dto) {
        log.info("Deleting object of class '{}' with id '{}'", dto.getClass(), dto.getId());
        E entity = getMapper().toEntity(dto);
        getRepository().delete(entity);
    }

    protected abstract JpaRepository<E, Integer> getRepository();
    protected abstract EntityMapper<D, E> getMapper();
}
