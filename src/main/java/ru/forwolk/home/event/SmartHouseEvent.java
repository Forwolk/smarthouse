package ru.forwolk.home.event;

import lombok.Getter;

import java.time.Instant;

@Getter
public class SmartHouseEvent {
    protected Instant called = Instant.now();
    protected String message;
    private final boolean audit;

    protected SmartHouseEvent(boolean audit) {
        this.audit = audit;
    }
}
