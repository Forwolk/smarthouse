import React, {useEffect, useState} from 'react';
import Modal from 'react-modal';
import {Button, Fab} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import {Delete, Visibility} from '@mui/icons-material';
import {atomDark as syntaxHighlighterStyle} from 'react-syntax-highlighter/dist/esm/styles/prism';
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {observer} from "mobx-react-lite";

const API_URL = "http://localhost:8080/";

const Scripts = () => {
    const [scripts, setScripts] = useState([]);
    const [selectedScript, setSelectedScript] = useState(null);
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
    const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
    const [editedScript, setEditedScript] = useState('');
    const [isEditing, setIsEditing] = useState(false); // Добавляем состояние для определения, редактируется ли скрипт
    const [scriptName, setScriptName] = useState(''); // Add scriptName state for script title

    const theme = createTheme({
        components: {
            MuiButton: {
                styleOverrides: {
                    root: {
                        margin: '0.5rem',
                        backgroundColor: '#007BFF',
                        color: 'white',
                        '&:hover': {
                            backgroundColor: '#0056b3',
                        },
                    },
                },
            },
        },
    });

    useEffect(() => {
        // Функция для получения списка скриптов
        const fetchScripts = async () => {
            try {
                const response = await fetch(`${API_URL}/api/script`);
                const data = await response.json();
                if (data.response) {
                    setScripts(data.response);
                }
            } catch (error) {
                console.error('Error fetching scripts:', error);
            }
        };

        fetchScripts();
    }, []);

    // Функция для открытия модального окна редактирования/просмотра скрипта
    const openEditViewModal = (script) => {
        setSelectedScript(script);
        setEditedScript(script.source); // Устанавливаем текущий текст скрипта
        setIsEditModalOpen(true);
        setIsEditing(false); // При открытии окна устанавливаем режим просмотра
    };

    // Функция для закрытия модального окна редактирования/просмотра скрипта
    const closeEditViewModal = () => {
        if (isEditing) {
            // Если окно находится в режиме редактирования, запрашиваем подтверждение перед закрытием
            const confirmClose = window.confirm('Вы уверены, что хотите закрыть окно без сохранения изменений?');
            if (!confirmClose) return;
        }
        setSelectedScript(null);
        setIsEditModalOpen(false);
        setIsEditing(false); // При закрытии окна устанавливаем режим просмотра
    };

    // Функция для переключения между режимами просмотра и редактирования
    const toggleEditViewMode = () => {
        setIsEditing((prev) => !prev); // Инвертируем состояние режима редактирования
    };

    // Функция для обработки изменений в тексте редактирования
    const handleScriptChange = (event) => {
        setEditedScript(event.target.value);
    };

    // Функция для открытия модального окна подтверждения удаления
    const openDeleteModal = (script) => {
        setSelectedScript(script);
        setIsDeleteModalOpen(true);
    };

    // Функция для закрытия модального окна подтверждения удаления
    const closeDeleteModal = () => {
        setSelectedScript(null);
        setIsDeleteModalOpen(false);
    };

    // Функция для удаления скрипта
    const handleDeleteScript = async () => {
        try {
            // Отправить запрос на удаление скрипта на бэкенд
            const response = await fetch(`${API_URL}/api/script`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    request: selectedScript,
                }),
            });

            const data = await response.json();
            if (data.status && data.status.code === 0) {
                // Успешно удалено, обновить список скриптов
                setScripts((prevScripts) =>
                    prevScripts.filter((script) => script.id !== selectedScript.id)
                );
                closeDeleteModal();
            }
        } catch (error) {
            console.error('Error deleting script:', error);
        }
    };

    const handleSelectedScriptNameChange = (event) => {
        setSelectedScript({
            "id": selectedScript.id,
            "name": event.target.value,
            "source": selectedScript.source
        })
    }

    const handleSelectedScriptSourceChange = (event) => {
        setSelectedScript({
            "id": selectedScript.id,
            "name": selectedScript.name,
            "source": event.target.value
        })
    }

    // Функция для открытия модального окна создания скрипта
    const openCreateModal = () => {
        setIsCreateModalOpen(true);
        setSelectedScript({})
    };

    // Функция для закрытия модального окна создания скрипта
    const closeCreateModal = () => {
        setIsCreateModalOpen(false);
        setEditedScript("");
        setScriptName("");
    };

    // Функция для сохранения скрипта
    const handleSaveCreateScript = async () => {
        let success = saveScript({
            name: scriptName, // Используем значение из состояния scriptName в качестве названия скрипта
            source: editedScript, // Используем текст из textarea в качестве исходного кода скрипта
        }, 'POST');
        if (success) {
            closeCreateModal(); // Закрываем модальное окно после успешной отправки
        }
    };

    // Функция для сохранения скрипта
    const handleSaveEditScript = async () => {
        let success = saveScript(selectedScript, 'PUT');
        if (success) {
            setSelectedScript(null);
            setIsEditModalOpen(false);
            setIsEditing(false);
        }
    };

    const saveScript = async (script, method) => {
        try {
            // Отправить запрос на создание скрипта на бэкенд
            const response = await fetch(`${API_URL}/api/script`, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json', // Добавляем заголовок для указания принимаемого формата ответа
                },
                body: JSON.stringify({
                    request: {
                        id: script.id, //Идентификтор скрипта
                        name: script.name, // Используем значение из состояния scriptName в качестве названия скрипта
                        source: script.source, // Используем текст из textarea в качестве исходного кода скрипта
                    },
                }),
            });

            const data = await response.json();
            if (data.status && data.status.code === 0) {
                // Успешно создано, обновить список скриптов
                setScripts((prevScripts) =>
                    prevScripts.filter((script) => script.id !== selectedScript.id)
                );
                setScripts((prevScripts) => [...prevScripts, data.response]); // Добавляем новый скрипт в список
                return true;
            }
        } catch (error) {
            console.error('Error saving script:', error);
        }
        return false;
    }

// Функция для обработки изменений в поле ввода названия скрипта
    const handleScriptNameChange = (event) => {
        setScriptName(event.target.value);
    };


    // Функция для открытия модального окна подтверждения отмены создания скрипта
    const openCancelModal = () => {
        const confirmCancel = window.confirm('Вы уверены, что хотите отменить создание скрипта?');
        if (confirmCancel) {
            closeCreateModal();
        }
    };

    return (
        <div>
            <h1>Раздел "Скрипты"</h1>
            <table>
                <thead>
                <tr>
                    <th>ID скрипта</th>
                    <th>Название</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                {scripts.map((script) => (
                    <tr key={script.id}>
                        <td>{script.id}</td>
                        <td>{script.name}</td>
                        <td>
                            <Button onClick={() => openEditViewModal(script)}>
                                <Visibility/>
                            </Button>
                            <Button onClick={() => openDeleteModal(script)}>
                                <Delete/>
                            </Button>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>

            {/* Кнопка для создания скрипта */}
            <Fab color="primary" aria-label="add" onClick={openCreateModal}
                 style={{position: 'fixed', bottom: '16px', right: '16px'}}>
                <AddIcon/>
            </Fab>

            {/* Объединенное модальное окно редактирования/просмотра скрипта */}
            <Modal
                isOpen={isEditModalOpen}
                onRequestClose={closeEditViewModal}
                style={{
                    overlay: {
                        backgroundColor: 'rgba(0, 0, 0, 0.6)',
                    },
                    content: {
                        width: '60%',
                        left: '20%',
                        height: '70%',
                        margin: '0 auto',
                        overflow: 'auto',
                        borderRadius: 10,
                        padding: 20,
                        boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)',
                    },
                }}
            >
                {selectedScript && (
                    <div>
                        <h2>{isEditing ? 'Редактирование' : 'Просмотр'} скрипта</h2>
                        <p>ID: {selectedScript.id}</p>
                        {isEditing ? (
                            <input
                                type="text"
                                value={selectedScript.name}
                                onChange={handleSelectedScriptNameChange}
                                placeholder="Введите название скрипта..."
                                style={{width: '95%', padding: '8px', borderRadius: '5px', border: '1px solid #ccc', marginBottom: 10}}
                            />
                        ) : (
                            <p>Название: {selectedScript.name}</p>
                        )
                        }
                        {isEditing ? (
                            <>
                                <textarea
                                    value={selectedScript.source}
                                    onChange={handleSelectedScriptSourceChange}
                                    style={{width: '97%', height: '300px'}}
                                />
                                <Button variant="contained" color="primary" onClick={handleSaveEditScript}>Сохранить</Button>{' '}
                                <Button variant="contained" color="primary" onClick={() => console.log('Проверить')}>Проверить</Button>{' '}
                            </>
                        ) : (
                            <>
                                <SyntaxHighlighter
                                    language="lua"
                                    style={syntaxHighlighterStyle}
                                    customStyle={{width: '95%', height: '300px'}} // Задаем размеры textarea
                                    showLineNumbers // Отображаем номера строк
                                    wrapLines // Переносим строки, чтобы текст не выходил за пределы textarea
                                    lineProps={{style: {whiteSpace: 'pre-wrap'}}} // Разрешаем переносы строк внутри строки кода
                                    codeTagProps={{style: {fontFamily: 'inherit'}}} // Задаем наследование шрифта для кода
                                >
                                    {editedScript}
                                </SyntaxHighlighter>
                                <ThemeProvider theme={theme}>
                                    <Button onClick={() => console.log('Запустить скрипт')}>Запустить</Button>
                                    <Button onClick={toggleEditViewMode}>Редактировать</Button>
                                </ThemeProvider>
                            </>
                        )}
                        <Button variant="contained" color="warning" onClick={closeEditViewModal}>Закрыть</Button>{' '}
                    </div>
                )}
            </Modal>

            {/* Модальное окно подтверждения удаления */}
            <Modal isOpen={isDeleteModalOpen} onRequestClose={closeDeleteModal}
                   style={{
                       content: {
                           width: '40%', // Устанавливаем 40% ширины окна
                           height: 180,
                           top: '50%', // Позиционируем окно по вертикали
                           left: '50%', // Позиционируем окно по горизонтали
                           transform: 'translate(-50%, -50%)', // Центрируем окно
                       },
                       overlay: {
                           backgroundColor: 'rgba(0, 0, 0, 0.5)', // Затеняем задний фон
                       }
                   }}
            >
                {selectedScript && (
                    <div>
                        <h2>Удаление скрипта</h2>
                        <p>Вы уверены, что хотите удалить скрипт с ID {selectedScript.id}?</p>
                        <Button variant="contained" color="warning" onClick={handleDeleteScript}>Удалить</Button>{' '}
                        <Button variant="contained" color="primary" onClick={closeDeleteModal}>Отмена</Button>{' '}
                    </div>
                )}
            </Modal>

            {/* Модальное окно создания скрипта */}
            <Modal isOpen={isCreateModalOpen} onRequestClose={openCancelModal}
                   style={{
                       content: {
                           width: '60%',
                           top: '50%',
                           left: '55%',
                           transform: 'translate(-50%, -50%)',
                           height: '80%',
                           maxHeight: '80%',
                           display: 'flex',
                           flexDirection: 'column',
                           justifyContent: 'space-between',
                           padding: '20px',
                           borderRadius: '10px',
                           boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)',
                       },
                       overlay: {
                           backgroundColor: 'rgba(0, 0, 0, 0.5)',
                           zIndex: '1000',
                       }
                   }}
            >
                {selectedScript && (
                    <div>
                        <div>
                            <h2>Создание скрипта</h2>
                            <div style={{marginBottom: '10px'}}>
                                <label style={{marginBottom: '5px'}}>Название скрипта:</label>
                                <input
                                    type="text"
                                    value={scriptName}
                                    onChange={handleScriptNameChange}
                                    placeholder="Введите название скрипта..."
                                    style={{width: '100%', padding: '8px', borderRadius: '5px', border: '1px solid #ccc'}}
                                />
                            </div>
                            <textarea
                                value={editedScript}
                                onChange={handleScriptChange}
                                style={{
                                    width: '100%',
                                    minHeight: '70%',
                                    padding: '8px',
                                    borderRadius: '5px',
                                    border: '1px solid #ccc'
                                }}
                                placeholder="Введите исходный код скрипта здесь..."
                            />
                        </div>
                        <div style={{textAlign: 'right'}}>
                            <Button variant="contained" color="primary" onClick={handleSaveCreateScript}>Сохранить</Button>{' '}
                            <Button variant="contained" color="warning" onClick={openCancelModal}>Отмена</Button>{' '}
                        </div>
                    </div>
                )}
            </Modal>
        </div>
    );
};

export default observer(Scripts);
