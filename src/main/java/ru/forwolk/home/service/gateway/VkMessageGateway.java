package ru.forwolk.home.service.gateway;

import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.forwolk.home.config.ServiceConfiguration;
import ru.forwolk.home.config.SettingsKeys;
import ru.forwolk.home.service.AuditService;

@Slf4j
@Service
public class VkMessageGateway implements MessageGateway {
    @Setter(onMethod_= @Autowired)
    private GroupActor groupActor;
    @Setter(onMethod_= @Autowired)
    private VkApiClient vkApiClient;
    @Setter(onMethod_= @Autowired)
    private ServiceConfiguration serviceConfiguration;
    @Setter(onMethod_= @Autowired)
    private AuditService auditService;

    public void sendMessage(String message) {
        sendMessage(8303061, message);
    }

    @Override
    public void sendMessage(int id, String message) {
        try {
            vkApiClient.messages().send(groupActor).userId(id).message(message).execute();
            auditService.audit("VK Message send", message);
        } catch (ApiException | ClientException e) {
            log.error("Error occurred while message sending", e);
        }
    }
}
