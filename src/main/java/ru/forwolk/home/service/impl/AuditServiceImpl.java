package ru.forwolk.home.service.impl;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.forwolk.home.domain.AuditMessage;
import ru.forwolk.home.repository.AuditMessageRepository;
import ru.forwolk.home.service.AuditService;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class AuditServiceImpl implements AuditService {

    @Setter(onMethod_= @Autowired)
    private AuditMessageRepository auditMessageRepository;

    public void audit(AuditMessage auditMessage) {
        log.info("[Audit] {}", auditMessage);
        auditMessageRepository.save(auditMessage);
    }

    public void audit(String eventType, String message) {
        audit(eventType, message, new HashMap<String, String>());
    }

    public void audit(String eventType, String message, Map<String, String> parameters) {
        AuditMessage auditMessage = new AuditMessage();
        auditMessage.setEventType(eventType);
        auditMessage.setMessage(message);
        auditMessage.setParameters(parameters);
    }
}
