package ru.forwolk.home.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.forwolk.home.domain.Property;

import java.util.Optional;

public interface PropertyRepository extends JpaRepository<Property, Integer> {

    Optional<Property> findByKey(String key);
}
