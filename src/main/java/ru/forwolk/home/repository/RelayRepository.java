package ru.forwolk.home.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.forwolk.home.domain.Relay;

public interface RelayRepository extends JpaRepository<Relay, Integer> {
}
