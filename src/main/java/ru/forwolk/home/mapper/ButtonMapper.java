package ru.forwolk.home.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.forwolk.home.domain.Button;
import ru.forwolk.home.web.dto.ButtonDto;

@Mapper(componentModel = "spring", uses = {ScriptMapper.class})
public interface ButtonMapper extends EntityMapper<ButtonDto, Button> {

    @Mapping(source = "script.id", target = "scriptId")
    @Mapping(source = "script.name", target = "scriptName")
    ButtonDto toDto(Button button);

    @Mapping(source = "scriptId", target = "script")
    Button toEntity(ButtonDto buttonDTO);

    default Button fromId(Integer id) {
        if (id == null) {
            return null;
        }
        Button button = new Button();
        button.setId(id);
        return button;
    }
}
