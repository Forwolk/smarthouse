package ru.forwolk.home.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;

@Slf4j
@Service
public class GatewayService {
    @Value("${smarthouse.gateway.controller.address}")
    private String ip;
    private static final String CONTROLLER_REQUEST_URL_TEMPLATE = "http://{0}/sec/?cmd={1}:{2}";

    public void activate(int port) {
        log.info("Activating module on port {}", port);
        makeRequest(port, 1);
    }

    public void deactivate(int port) {
        log.info("Deactivating module on port {}", port);
        makeRequest(port, 0);
    }

    private void makeRequest(int port, int command) {
        String urlStr = MessageFormat.format(CONTROLLER_REQUEST_URL_TEMPLATE, ip, port, command);
        try {
            log.info("Request: '{}'", urlStr);
            URL url = new URL(urlStr);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(3000);//3 seconds for request
            int responseCode = connection.getResponseCode();
            if (responseCode != 200) {
                log.warn("Response code is: {}", responseCode);
            }
        } catch (IOException e) {
            log.error("Error occurred while creating request", e);
            throw new RuntimeException(e.getMessage());
        }
    }
}
