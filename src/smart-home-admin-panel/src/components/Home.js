import React from 'react';

const Home = () => {
    return (
        <div>
            <h1>Добро пожаловать в систему умного дома!</h1>
            <p>Здесь будет отображаться общая информация о состоянии системы и метриках.</p>
        </div>
    );
};

export default Home;