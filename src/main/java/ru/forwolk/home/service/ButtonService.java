package ru.forwolk.home.service;

public interface ButtonService {

    /**
     * Реакция на нажатие кнопки
     *
     * @param port порт
     * @param mode 1 -- при отпускании, 2 -- при длительном нажатии
     * @param count -- количество нажатий
     * @param click 1 -- однократное нажатие, 2 -- двойной клик
     */
    void onClick(int port, int mode, int count, int click);
}
