package ru.forwolk.home.service.crud;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.forwolk.home.domain.Script;
import ru.forwolk.home.mapper.ScriptMapper;
import ru.forwolk.home.repository.ScriptRepository;
import ru.forwolk.home.service.AbstractCrudService;
import ru.forwolk.home.web.dto.ScriptDto;

@Service
public class ScriptCrudService extends AbstractCrudService<ScriptDto, Script> {
    @Setter(onMethod_= @Autowired)
    @Getter
    private ScriptRepository repository;
    @Setter(onMethod_= @Autowired)
    @Getter
    private ScriptMapper mapper;
}
