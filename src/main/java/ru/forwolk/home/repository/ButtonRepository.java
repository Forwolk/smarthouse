package ru.forwolk.home.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.forwolk.home.domain.AuditMessage;
import ru.forwolk.home.domain.Button;

import java.util.Optional;

public interface ButtonRepository extends JpaRepository <Button, Integer> {

    Optional<Button> findButtonByPort(int port);
}
