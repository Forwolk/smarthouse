package ru.forwolk.home.web.dto;

import lombok.Data;
import ru.forwolk.home.domain.Identifiable;

@Data
public class TaskDto implements Identifiable {
    private Integer id;
    private String name;
    private String description;
    private String cron;
    private String scriptId;
    private String scriptName;
}
