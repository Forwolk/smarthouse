package ru.forwolk.home.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;

@Data
@Entity
@Table(name = "sh_audit_message")
public class AuditMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Date created = new Date();
    private String eventType;
    private String message;

    @ElementCollection
    @MapKeyColumn(table = "sh_audit_message_parameters", name="key")
    @Column(name="value")
    private Map<String, String> parameters;
}
