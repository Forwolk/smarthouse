package ru.forwolk.home.web.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseDto<T> implements Serializable {
    private T response;
    private long timestamp = System.currentTimeMillis();
    private StatusDto status;
}
