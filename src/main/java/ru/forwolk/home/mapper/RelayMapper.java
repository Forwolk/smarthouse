package ru.forwolk.home.mapper;

import org.mapstruct.Mapper;
import ru.forwolk.home.domain.Relay;
import ru.forwolk.home.web.dto.RelayDto;

@Mapper(componentModel = "spring", uses = {})
public interface RelayMapper extends EntityMapper<RelayDto, Relay> {

    default Relay fromId(Integer id) {
        if (id == null) {
            return null;
        }
        Relay relay = new Relay();
        relay.setId(id);
        return relay;
    }
}
